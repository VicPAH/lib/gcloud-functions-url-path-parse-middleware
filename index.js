const _ = require('lodash');
const { pathToRegexp } = require('path-to-regexp');

const { NotFoundError } = require('@VicPAH/gcloud-functions-http-errors-middleware/errors');

const defaultOpts = {
  sensitive: true,
};

exports.parsePath = (path, opts = {}) => {
  // TODO repeats won't work with our method of zipping
  // `keys` and `match` from the exec
  if (path.indexOf('*') >= 0 || path.indexOf('+') >= 0) {
    throw new Error('Repeat parameters not implemented');
  }

  // Build
  const keys = [];
  const pathRe = pathToRegexp(path, keys, _.defaults(opts, defaultOpts));

  return (req, res, next) => {
    const match = pathRe.exec(req.url);

    // Bare match
    if (!match) throw new NotFoundError();

    // Write urlParams
    if (keys.length > 0) {
      req.urlParams = _.chain(_.zip(keys, match.slice(1)))
        .map(([keydef, value]) => [keydef.name, value])
        .fromPairs()
        .value();
    } else {
      req.urlParams = {};
    }

    // Continue
    return next();
  };
};

exports.pathRouter = (pathsAndHandlers, opts = {}) => {
  throw new Error('This is the first use of pathRouter. Make sure to test it thoroughly');
  const list = pathsAndHandlers.map( // eslint-disable-line no-unreachable
    ([path, handler]) => ([
      exports.parsePath(path, opts),
      handler,
    ]),
  );
  return (req, res, next) => {
    for (const [pathHandler, nextHandler] of list) {
      let doHandler = false;
      try {
        pathHandler(req, res, () => {});
        doHandler = true;
      } catch (err) {
        if (err instanceof NotFoundError) {
          continue;
        }
        throw err;
      }
      if (doHandler) {
        return nextHandler(req, res, next);
      }
    }
    throw new NotFoundError();
  };
};
