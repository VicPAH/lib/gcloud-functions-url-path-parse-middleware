const { NotFoundError } = require('@VicPAH/gcloud-functions-http-errors-middleware/errors');

const { parsePath, pathRouter } = require('.');

describe('parsePath()', () => {
  test('matching static URL produces no error', () => {
    expect(() => parsePath('/test/path')({ url: '/test/path' }, {}, () => {})).not.toThrowError();
  });
  test('non-matching static URL produces NotFoundError', () => {
    expect(() => parsePath('/test/path')({ url: '/different' }, {}, () => {})).toThrowError(NotFoundError);
  });
  test('sets urlParams on req', () => {
    const req = { url: '/test/thevalue' };
    parsePath('/test/:thevar')(req, {}, () => {});
    expect(req).toEqual(expect.objectContaining({
      urlParams: {
        thevar: 'thevalue',
      },
    }));
  });
  test('optional params', () => {
    const fn = parsePath('/test/:thevar/:optvar?');

    // Doesn't have to exist
    const req1 = { url: '/test/thevalue' };
    fn(req1, {}, () => {});
    expect(req1).toEqual(expect.objectContaining({
      urlParams: {
        thevar: 'thevalue',
      },
    }));

    // Parsed if present
    const req2 = { url: '/test/thevalue/thesecond' };
    fn(req2, {}, () => {});
    expect(req2).toEqual(expect.objectContaining({
      urlParams: {
        thevar: 'thevalue',
        optvar: 'thesecond',
      },
    }));
  });
  test('calls next', async () => {
    const nextSuccess = jest.fn(() => Promise.resolve());
    const failErr = new Error('testerr');
    const nextFail = jest.fn(() => Promise.reject(failErr));
    const fn = parsePath('/t');

    await expect(fn({ url: '/t' }, {}, nextSuccess)).resolves;
    expect(nextSuccess).toHaveBeenCalled();

    await expect(fn({ url: '/t' }, {}, nextFail)).rejects.toBe(failErr);
    expect(nextFail).toHaveBeenCalled();
  });

  test('case sensitive by default', () => {
    expect(() => parsePath('/t')({ url: '/T' }, {}, () => {})).toThrowError(NotFoundError);
  });
  test('case sensitivity overridable', () => {
    expect(() => parsePath('/t', { sensitive: false })({ url: '/T' }, {}, () => {})).not.toThrowError();
  });

  test('error on zero or more', () => {
    expect(() => parsePath('/test/:p*')).toThrowError(/repeat parameters not implemented/i);
  });
  test('error on one or more', () => {
    expect(() => parsePath('/test/:p+')).toThrowError(/repeat parameters not implemented/i);
  });
});

describe('pathRouter() first use', () => {
  test('first use error', () => {
    expect(() => pathRouter()).toThrowError('first use');
  });
});
describe.skip('pathRouter()', () => {
  test('routes a single path correctly', () => {
    const hFn = jest.fn();
    const fn = pathRouter([['/t', hFn]]);
    fn({ url: '/t' });
    expect(hFn).toHaveBeenCalled();
  });
  test('routes multiple paths correctly', () => {
    const firstFn = jest.fn();
    const secondFn = jest.fn();
    const fn = pathRouter([
      ['/a', firstFn],
      ['/t', secondFn],
    ]);
    fn({ url: '/t' });
    expect(firstFn).not.toHaveBeenCalled();
    expect(secondFn).toHaveBeenCalled();
  });
  test('error on no paths found', () => {
    const firstFn = jest.fn();
    const secondFn = jest.fn();
    const fn = pathRouter([
      ['/a', firstFn],
      ['/b', secondFn],
    ]);
    expect(() => fn({ url: '/t' })).toThrowError(NotFoundError);
    expect(firstFn).not.toHaveBeenCalled();
    expect(secondFn).not.toHaveBeenCalled();
  });
});
